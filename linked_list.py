#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 10:18:49 2020

@author: zzyang
"""

"""
the linked list is special list which the elements has the head and the tail point to the next element.
1. the class Node is for building node that contains the value and the pointer to the next element
2. the class Linked_list is for building the linked list and insert the value
    - initialize the linked list and the head is the node, which is None at beginning
    - the creat_list function is used to creat the linked list. The index is used as the
      reference of postion. When the head is None, the node will be put at beginning. Otherwise
      the index as a reference, will go to the next element until the end of list and put the node at end
    - the insert function inserts the new node in the second place of the linked list. The index as reference 
      will begin at the first place and insert the given node at the second node

Time Complexity
As shown in the second half of the code, there are three different size of list, the insert function is used 
to insert 50 elements in the list. The time used is the time of inseting 50 elements divided by 50. 
This is because the time of inserting is very small and the average can help to aviod problems when evaluating.
The insertion is not related to the size of the size, also can be found in the graph. The time complexity is constant
The time complexity is O(1)
"""

class Node:
    def __init__(self,value):
        self.value = value
        self.next = None
    
class linked_list:
    def __init__(self,node = None):
        self.head = node
        
    def creat_list(self,value):
        node = Node(value)
        index = self.head
        if self.head == None:
            self.head = node
        else:
            while index.next != None:
                index = index.next
            index.next = node

    def insert(self,value):
        node = Node(value)
        index = self.head
        node.next = index.next
        index.next = node
       
    def display(self):
        index = self.head
        while index != None:
            print(index.value, end=" ")
            index = index.next

import time
import matplotlib.pyplot as plt

list_1=linked_list()
list_2=linked_list()
list_3=linked_list()
length = [10,20,30]
used_time=[]

for n in range(10):
    list_1.creat_list(n)
start = time.time()
for i in range(50):
    list_1.insert('A')
end = time.time()
used_time.append((end-start)/50)

for n in range(20):
    list_2.creat_list(n)
start = time.time()
for i in range(50):
    list_2.insert('A')
end = time.time()
used_time.append((end-start)/50)

for n in range(30):
    list_3.creat_list(n)
start = time.time()
for i in range(50):
    list_3.insert('A')
end = time.time()
used_time.append((end-start)/50)

print(used_time)

x = length
y = used_time
plt.title = 'Time complexity for insert in linked list'
plt.xlabel = 'Size of the list'
plt.ylabel = 'Time'
plt.ylim(0,0.00005)
plt.plot(x,y)
plt.show()

